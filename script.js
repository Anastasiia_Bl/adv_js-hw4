// fetch("https://ajax.test-danit.com/api/swapi/films")
//     .then(response => response.json())
//     .then(result => {
//         let filmsList = result;
//         console.log(filmsList);
//     });




// Для решения этой задачи, мы можем использовать функции для получения данных с сервера и для рендеринга полученной информации.

// Функция getFilms() будет отвечать за получение списка фильмов с сервера, а функция getCharacters() - за получение списка персонажей каждого фильма. Функция renderFilms() будет отвечать за рендеринг списка фильмов на странице, а функция renderCharacters() - за рендеринг списка персонажей для каждого фильма.

// Также мы будем использовать метод Promise.all() для выполнения запросов к серверу параллельно и получения всех данных одновременно.


// let charArr = ["https://ajax.test-danit.com/api/swapi/people/1","https://ajax.test-danit.com/api/swapi/people/2","https://ajax.test-danit.com/api/swapi/people/3","https://ajax.test-danit.com/api/swapi/people/4","https://ajax.test-danit.com/api/swapi/people/5"]
// Код робочий, але з помилкою, яка не заважає виведеню списку персонажей під фільмом
// const urlFilms = "https://ajax.test-danit.com/api/swapi/films";

// function getFilms(url) {
//     return fetch(url)
//       .then(response => response.json())
//       .catch((error) => console.log(error));
//   }
  
//   function getCharacters(charactersArray, filmItem) {
//     const characterPromises = charactersArray.map(url => fetch(url).then(response => response.json()));
//     return Promise.all(characterPromises)
//     .then(characters => renderCharacters(characters, filmItem))
//     .catch(error => console.log(error));
//   }
  
//   function renderFilms(films) {
//     const filmsList = document.createElement("ul");
//     films.forEach(film => {
//       const filmItem = document.createElement("li");
//       filmItem.innerHTML = `<strong>Episode ${film.episodeId}: ${film.name}</strong><br>${film.openingCrawl}`;
//       const charactersList = document.createElement("ul");
//       filmItem.appendChild(charactersList);
//       filmsList.appendChild(filmItem);
//         getCharacters(film.characters, charactersList)
//           .then(characters => renderCharacters(characters))
//           .catch(error => console.log(error));
//       // filmsList.appendChild(filmItem);
//     });
//     document.body.appendChild(filmsList);
//   }
  
//   function renderCharacters(characters, filmItem) {
//     // const charactersList = document.createElement("ul");
//     const charactersList = filmItem;
//     characters.forEach(character => {
//       const characterItem = document.createElement("li");
//       characterItem.textContent = `${character.name}`;
//       charactersList.appendChild(characterItem);
//     });
//     // document.body.appendChild(charactersList);
//   }
  
//   getFilms(urlFilms)
//     .then(films => renderFilms(films))
//     .catch(error => console.log(error));















// function fetchData(url) {
//     return fetch(url).then(response => {
//       if (!response.ok) {
//         throw new Error(`HTTP error! status: ${response.status}`);
//       }
//       return response.json();
//     });
//   }
  
//   function renderFilmList(filmList) {
//     const filmListContainer = document.querySelector("#film-list");
  
//     filmList.forEach(film => {
//       const filmItem = document.createElement("li");
//       filmItem.textContent = `${film.episodeId}. ${film.name}`;
//       filmItem.addEventListener("click", () => {
//         fetchCharacters(film.characters, film.name);
//       });
  
//       filmListContainer.appendChild(filmItem);
//     });
//   }
  
//   function renderCharactersList(charactersList, filmTitle) {
//     const charactersListContainer = document.querySelector("#characters-list");
  
//     charactersList.forEach(character => {
//       const characterItem = document.createElement("li");
//       characterItem.textContent = character.name;
//       charactersListContainer.appendChild(characterItem);
//     });
  
//     const charactersTitle = document.createElement("h2");
//     charactersTitle.textContent = `Characters in ${filmTitle}:`;
//     charactersListContainer.prepend(charactersTitle);
//   }
  
//   function fetchCharacters(charactersUrls, filmTitle) {
//     Promise.all(charactersUrls.map(url => fetchData(url)))
//       .then(charactersList => renderCharactersList(charactersList, filmTitle))
//       .catch(error => console.error("Error fetching characters: ", error));
//   }
  
//   fetchData("https://ajax.test-danit.com/api/swapi/films")
//     .then(filmList => renderFilmList(filmList.results))
//     .catch(error => console.error("Error fetching films: ", error));










const urlFilms = "https://ajax.test-danit.com/api/swapi/films";

function getFilms(url) {
  return fetch(url)
    .then(response => response.json())
    .catch(error => console.log(error));
}

function getCharacters(charactersArray) {
  const characterPromises = charactersArray.map(url => fetch(url).then(response => response.json()));
  return Promise.all(characterPromises);
}

function renderFilms(films) {
  const filmsList = document.createElement("ul");
  films.forEach(({episodeId, name, openingCrawl, characters}) => {
    const filmItem = document.createElement("li");
    filmItem.innerHTML = `<strong>Episode ${episodeId}: ${name}</strong><br>${openingCrawl}`;
    const charactersList = document.createElement("ul");
    filmItem.appendChild(charactersList);
    filmsList.appendChild(filmItem);
    getCharacters(characters)
      .then(characters => renderCharacters(characters, charactersList))
      .catch(error => console.log(error));
  });
  document.body.appendChild(filmsList);
}

function renderCharacters(characters, charactersList) {
  characters.forEach(({name}) => {
    const characterItem = document.createElement("li");
    characterItem.textContent = `${name}`;
    charactersList.appendChild(characterItem);
  });
}

getFilms(urlFilms)
  .then(films => renderFilms(films))
  .catch(error => console.log(error));